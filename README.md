# Zeno Website

An infomational website for the Zeno code editor.

## Running

Ensure you have Python 3.6.8 and git installed. `python3` is the PATH to the Python 3.6.8 install.

```bash
# Clone the git repository
git clone https://gitlab.com/zeno-src/zeno-website/

# Enter newly-cloned repository
cd zeno-website

# Install the `pipenv` package
python3 -m pip install pipenv

# Run app.py with the pipenv package.
pipenv run python app.py
```
